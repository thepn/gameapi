package de.sebpas.api.group;

public class Group {
	private String chatPrefix  = null;
	private String nameTagSuffix = null;
	private String nameTagPrefix = null;
	private int index;
	private String permission;
	
	private boolean defaultGroup = false;
	
	public Group() {}

	public String getPermission(){
		return permission;
	}
	
	public void setPermission(String permission){
		this.permission = permission;
	}
	
	public String getChatPrefix() {
		return chatPrefix;
	}

	public void setChatPrefix(String chatPrefix) {
		this.chatPrefix = chatPrefix;
	}

	public String getNameTagSuffix() {
		return nameTagSuffix;
	}

	public void setNameTagSuffix(String suffix) {
		this.nameTagSuffix = suffix;
	}

	public String getNameTagPrefix() {
		return nameTagPrefix;
	}

	public void setNameTagPrefix(String nameTagPrefix) {
		this.nameTagPrefix = nameTagPrefix;
	}

	public boolean isDefaultGroup() {
		return defaultGroup;
	}

	public void setDefaultGroup(boolean defaultGroup) {
		this.defaultGroup = defaultGroup;
	}
	
	public int getIndex(){
		return index;
	}
	
	public void setIndex(int index){
		this.index = index;
	}
}
