package de.sebpas.api.group;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import de.sebpas.api.GameApi;

public class GroupManager implements Listener{
	private List<Group> groups;
	
	public GroupManager(FileConfiguration cfg){
		groups = new ArrayList<Group>();
		
		this.init(cfg);
	}
	
	public void setNameTagPrefix(Player p){
		Group g = GameApi.getGroupManager().getGroup(p);
		if(g == null){
			g = GameApi.getGroupManager().getDefaultGroup();
		}

		if(g != null){
			Scoreboard sm = p.getScoreboard();
			if(sm == null){
				sm = Bukkit.getScoreboardManager().getMainScoreboard();
			}
			Team t = sm.getTeam(g.getIndex() + g.getPermission());
			if(t == null){
				t = sm.registerNewTeam(g.getIndex() + g.getPermission());
			}
			t.setNameTagVisibility(NameTagVisibility.ALWAYS);
			t.setPrefix(ChatColor.translateAlternateColorCodes('&', g.getNameTagPrefix()));
			if(!t.getPlayers().contains(p))
				t.addPlayer(p);
		}
	}
	
	public void setNameTagPrefix(Player p, String suffix){
		Group g = GameApi.getGroupManager().getGroup(p);
		if(g == null){
			g = GameApi.getGroupManager().getDefaultGroup();
		}

		if(g != null){
			Scoreboard sm = p.getScoreboard();
			if(sm == null){
				sm = Bukkit.getScoreboardManager().getMainScoreboard();
			}
			Team t = sm.getTeam(g.getIndex() + g.getPermission());
			if(t == null){
				t = sm.registerNewTeam(g.getIndex() + g.getPermission());
			}
			t.setNameTagVisibility(NameTagVisibility.ALWAYS);
			t.setPrefix(ChatColor.translateAlternateColorCodes('&', g.getNameTagPrefix()));
			t.setSuffix(suffix);
			if(!t.getPlayers().contains(p))
				t.addPlayer(p);
		}
	}
	
	private void init(FileConfiguration cfg){
		if(cfg.getConfigurationSection("GameApi.group") == null){
			cfg.addDefault("GameApi.group.default.permission", "groups.default");
			cfg.addDefault("GameApi.group.default.chatprefix", "&a%NAME%&7");
			cfg.addDefault("GameApi.group.default.nametagsuffix", "&7");
			cfg.addDefault("GameApi.group.default.nametag", "&7");
			cfg.addDefault("GameApi.group.default.default", true);
			
			cfg.addDefault("GameApi.group.admin.permission", "groups.admin");
			cfg.addDefault("GameApi.group.admin.chatprefix", "&4Admin | %NAME%&7");
			cfg.addDefault("GameApi.group.admin.nametagsuffix", "&4");
			cfg.addDefault("GameApi.group.admin.nametag", "&4");
			cfg.addDefault("GameApi.group.admin.default", false);
			cfg.options().copyDefaults(true);
			GameApi.getInstance().saveConfig();
		}
		int index = 0;
		for(String s : cfg.getConfigurationSection("GameApi.group").getKeys(false)){
			Group g = new Group();
			g.setChatPrefix(cfg.getString("GameApi.group." + s + ".chatprefix"));
			g.setNameTagSuffix(cfg.getString("GameApi.group." + s + ".nametagsuffix"));
			g.setNameTagPrefix(cfg.getString("GameApi.group." + s + ".nametag"));
			g.setPermission(cfg.getString("GameApi.group." + s + ".permission"));
			g.setDefaultGroup(cfg.getBoolean("GameApi.group." + s + ".default"));
			g.setIndex(index); 
			index ++;
			if(g.getPermission() == null)
				throw new NullPointerException("Permission is null: " + s + " in config.yml");
			groups.add(g);
		}
	}
	
	public Group getDefaultGroup(){
		for(Group g : groups)
			if(g.isDefaultGroup())
				return g;
		return null;
	}
	
	public Group getGroup(Player p){
		for(Group t : groups)
			if(p.hasPermission(t.getPermission()) && !t.isDefaultGroup())
				return t;
		return getDefaultGroup();
	}
	
	public List<Group> getGroups(){
		return groups;
	}
	
	@EventHandler
	public void chat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		Group g = this.getGroup(p);
		if(g.getChatPrefix().equals("") || g.getChatPrefix() == null)
			return;
		if(g != null){
			if(p.hasPermission("GameApi.chat.colors")){
				e.setFormat(ChatColor.translateAlternateColorCodes('&', e.getMessage()));
			}
			e.setFormat(ChatColor.translateAlternateColorCodes('&', ChatColor.translateAlternateColorCodes('&', g.getChatPrefix().replace("%NAME%", p.getName()))) + "%2$s");
		}
	}
}
