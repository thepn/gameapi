package de.sebpas.api.nick;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerNickEvent extends Event{
	/**
	 * this event is fired when a player nicks themself
	 */
	
	private static final HandlerList handlers = new HandlerList();
	
	private final NickData nickData;
	private Player player;
	
	public PlayerNickEvent(NickData nickData, Player player){
		this.nickData = nickData;
		this.player = player;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList(){
		return handlers;
	}

	public NickData getNickData() {
		return nickData;
	}

	public Player getPlayer() {
		return player;
	}
}
