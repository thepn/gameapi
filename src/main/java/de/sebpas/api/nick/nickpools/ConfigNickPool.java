package de.sebpas.api.nick.nickpools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.sebpas.api.GameApi;
import de.sebpas.api.nick.INickPool;
import de.sebpas.api.nick.NickData;
import de.sebpas.api.util.exception.PlayerNotFoundException;
import de.sebpas.api.util.mojang.OnlineUUIDFetcher;

public class ConfigNickPool implements INickPool{

	private List<String> names;
	
	public ConfigNickPool(){
		names = new ArrayList<String>();
		this.reload();
	}
	
	public void reload(){
		names.clear();
		if(!GameApi.getInstance().getConfig().contains("GameApi.nick.nickfile")){
			GameApi.getInstance().getConfig().addDefault("GameApi.nick.nickfile", "nicks.yml");
			GameApi.getInstance().getConfig().options().copyDefaults(true);
			GameApi.getInstance().saveConfig();
		}
		
		String path = "plugins/GameApi/" + GameApi.getInstance().getConfig().get("GameApi.nick.nickfile");
		File file = new File(path);
		if(!file.exists()){
			this.createFile(file);
		}
		
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		cfg.addDefault("Names", names);
		cfg.options().copyDefaults(true);
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		names = cfg.getStringList("Names");
	}
	
	private void createFile(File file){
		if(!file.getParentFile().exists())
			createFile(file.getParentFile());
		if(file.isDirectory())
			file.mkdirs();
		else
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	@Override
	public NickData getRandomNick() throws PlayerNotFoundException {
		if(names.size() == 0)
			throw new PlayerNotFoundException("null");
		String name = names.get(new Random().nextInt(names.size()));
		return new NickData(name, OnlineUUIDFetcher.getInstance().getUUID(name));
	}
}
