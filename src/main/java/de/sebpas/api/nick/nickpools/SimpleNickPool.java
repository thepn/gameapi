package de.sebpas.api.nick.nickpools;

import java.util.Random;

import de.sebpas.api.nick.INickPool;
import de.sebpas.api.nick.NickData;
import de.sebpas.api.util.exception.PlayerNotFoundException;
import de.sebpas.api.util.mojang.OnlineUUIDFetcher;

public class SimpleNickPool implements INickPool{

	private String[] nickNames = new String[] {
		"md5",
		"thepn",
		"Notch",
		"kp3",
		"sebiba_"
	};
	
	@Override
	public NickData getRandomNick() throws PlayerNotFoundException {
		String nick = nickNames[new Random().nextInt(nickNames.length)];
		return new NickData(nick, OnlineUUIDFetcher.getInstance().getUUID(nick));
	}
	
}
