package de.sebpas.api.nick;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerNickRemovedEvent extends Event{
	/**
	 * this event is fired when a player's nickname was removed 
	 */
	
	private static final HandlerList handlers = new HandlerList();
	
	private final NickData nickData;
	private Player player;
	
	public PlayerNickRemovedEvent(NickData nickData, Player player){
		this.nickData = nickData;
		this.player = player;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList(){
		return handlers;
	}

	public NickData getOldNickData() {
		return nickData;
	}

	public Player getPlayer() {
		return player;
	}
}
