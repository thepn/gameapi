package de.sebpas.api.nick;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.FieldAccessException;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;

import de.sebpas.api.GameApi;
import de.sebpas.api.NickManager;
import de.sebpas.api.util.config.Config;
import de.sebpas.api.util.exception.PlayerNotFoundException;

public class NickListener implements Listener{
	public NickListener(){
		Bukkit.getPluginManager().registerEvents(this, GameApi.getInstance());
	}
	public void start(){
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(GameApi.getInstance(), PacketType.Play.Server.NAMED_ENTITY_SPAWN,
				PacketType.Play.Server.PLAYER_INFO,
				PacketType.Play.Server.CHAT,
				PacketType.Play.Server.SCOREBOARD_TEAM,
				PacketType.Play.Server.SCOREBOARD_OBJECTIVE
				){
		@SuppressWarnings("unchecked")
		public void onPacketSending(PacketEvent event){
			if(event.getPlayer().hasPermission("nick")){
				return;
			}
			if(event.getPacketType() == PacketType.Play.Server.PLAYER_INFO){
				if(!GameApi.getNickManager().isNicked(event.getPacket().getPlayerInfoDataLists().read(0).get(0).getProfile().getUUID()))
					return;
				List<PlayerInfoData> newPlayerInfo = new ArrayList<PlayerInfoData>();
				for (PlayerInfoData playerInfo : (List<PlayerInfoData>) event.getPacket().getPlayerInfoDataLists().read(0)){
					if(GameApi.getNickManager().isNicked(playerInfo.getProfile().getName())){
						NickData data = GameApi.getNickManager().getNickName(playerInfo.getProfile().getName());
						if(data == null)
							return;
						WrappedGameProfile gp = data.getWrappedGameProfile();
						WrappedChatComponent displayName = null;
						if(playerInfo.getDisplayName() != null)
							 displayName = WrappedChatComponent.fromJson(playerInfo.getDisplayName().getJson().replace(playerInfo.getProfile().getName(), data.getNickName()));
						PlayerInfoData playerInfoNew = new PlayerInfoData(gp, playerInfo.getLatency(), playerInfo.getGameMode(), displayName);
						newPlayerInfo.add(playerInfoNew);
					}else{
						newPlayerInfo.add(playerInfo);
					}
				}
				event.getPacket().getPlayerInfoDataLists().write(0, newPlayerInfo);
			}
			if(event.getPacketType() == PacketType.Play.Server.NAMED_ENTITY_SPAWN){
				if(!GameApi.getNickManager().isNicked(event.getPacket().getUUIDs().read(0)))
					return;
				NickData data = GameApi.getNickManager().getNickName(event.getPacket().getUUIDs().read(0));
				if(data == null)
					return;
				event.getPacket().getUUIDs().write(0, data.getUuid());
			}
			
			if(event.getPacketType() == PacketType.Play.Server.SCOREBOARD_TEAM){
				try {
					Collection<String> newList = event.getPacket().getSpecificModifier(Collection.class).read(0).getClass().newInstance();
					for(String s : (List<String>) event.getPacket().getSpecificModifier(Collection.class).read(0)){
						if(GameApi.getNickManager().isNicked(s)){
							newList.add(GameApi.getNickManager().getNickName(s).getNickName());
						}else{
							newList.add(s);
						}
					}
					event.getPacket().getSpecificModifier(Collection.class).write(0, newList);
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (FieldAccessException e) {
					e.printStackTrace();
				}
			}
		}});
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onPlayerChatTabComplete(PlayerChatTabCompleteEvent e){
		String token = e.getLastToken().toLowerCase();
		Collection<String> matches = e.getTabCompletions();
		for(Player p : GameApi.getNickManager().getNickedPlayers()){
			if(p.getName().toLowerCase().startsWith(token)){
				if(p.hasPermission("nick")){
					matches.remove(p.getName());
					matches.add(GameApi.getNickManager().getNickName(p.getUniqueId()).getNickName());
				}
			}
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		if(GameApi.getNickManager().isNicked(e.getPlayer())){
			GameApi.getNickManager().unNick(e.getPlayer(), false);
		}
	}
	
	@EventHandler
	public void onJoin(PlayerLoginEvent e){
		final Player p = e.getPlayer();
		if(e.getPlayer().hasPermission("nick")){
			if(NickManager.autoNick && !Config.isApiOnly()){
				new BukkitRunnable() {
					
					@Override
					public void run() {
						if(GameApi.getNickManager().getNickPool() != null){
							try {
								GameApi.getNickManager().nick(p, GameApi.getNickManager().getNickPool().getRandomNick());
								p.sendMessage("�7[�5Nick�7]: �4Du bist nun als �8" + GameApi.getNickManager().getNickName(p.getName()).getNickName() + " �4genickt.");
							} catch (PlayerNotFoundException e) {
								e.printStackTrace();
								p.sendMessage("�7[�5Nick�7]: �4Fehler: " + e.getMessage());
							}
						}
					}
				}.runTaskLater(GameApi.getInstance(), 5);
			}
		}
	}
}
