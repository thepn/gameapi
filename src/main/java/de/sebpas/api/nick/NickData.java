package de.sebpas.api.nick;

import java.io.IOException;
import java.util.UUID;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import de.sebpas.api.util.exception.PlayerNotFoundException;
import de.sebpas.api.util.mojang.GameProfileFetcher;

public class NickData {
	private String nickName;
	private UUID uuid;
	private GameProfile gameProfile;
	
	public NickData(String nickName, UUID uuid) throws PlayerNotFoundException {
		this.nickName = nickName;
		this.uuid = uuid;
		try {
			this.gameProfile = GameProfileFetcher.fetch(uuid);
			if(gameProfile == null)
				throw new PlayerNotFoundException(uuid);
		} catch (IOException e) {
			e.printStackTrace();
			throw new PlayerNotFoundException(uuid);
		}
	}

	public WrappedGameProfile getWrappedGameProfile(){
		WrappedGameProfile gp = new WrappedGameProfile(uuid, nickName);
		if(gameProfile == null)
			return gp;
		for(String s : gameProfile.getProperties().keySet()){
			if(gp.getProperties().containsKey(s)){
				System.out.println(s + "; " + gp.getProperties().get(s));
			}else{
				for(Property p : gameProfile.getProperties().get(s))
					gp.getProperties().put(s, new WrappedSignedProperty(p.getName(), p.getValue(), p.getSignature()));
			}
		}
		return gp;
	}
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
}
