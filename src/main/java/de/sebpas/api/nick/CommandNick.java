package de.sebpas.api.nick;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.sebpas.api.GameApi;
import de.sebpas.api.util.exception.PlayerNotFoundException;
import de.sebpas.api.util.mojang.OnlineUUIDFetcher;


public class CommandNick implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(!p.hasPermission("system.nick")){
				p.sendMessage(GameApi.error + "Dazu bist du nicht berechtigt!");
				return true;
			}
			
			if(args.length == 0){
				GameApi.getNickManager().unNick(p, false);
				p.sendMessage("�7[�5Nick�7]: �4Du bist nun nicht mehr genickt.");
				return true;
			}
			
			String displayName = ChatColor.translateAlternateColorCodes('&', args[0]);
			String nick = ChatColor.stripColor(displayName);
			try {
				GameApi.getNickManager().nick(p, nick, OnlineUUIDFetcher.getInstance().getUUID(nick));
			} catch (PlayerNotFoundException e) {
				e.printStackTrace();
				p.sendMessage("�7[�5Nick�7]: �4Fehler: " + e.getMessage());
				return true;
			}
			p.sendMessage("�7[�5Nick�7]: �4Du bist nun als �8 " + displayName + "�4 genickt.");
		}
		return true;
	}

}
