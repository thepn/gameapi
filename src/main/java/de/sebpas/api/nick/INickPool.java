package de.sebpas.api.nick;

import de.sebpas.api.util.exception.PlayerNotFoundException;

public interface INickPool {
	
	/**
	 * returns a random generated nickdata instance
	 * @throws PlayerNotFoundException 
	 */
	NickData getRandomNick() throws PlayerNotFoundException;
}
