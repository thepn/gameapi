package de.sebpas.api;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import de.sebpas.api.gamesession.CommandGameID;
import de.sebpas.api.gamesession.GameSession;
import de.sebpas.api.gamesession.GameSessionListener;
import de.sebpas.api.group.GroupManager;
import de.sebpas.api.nick.CommandNick;
import de.sebpas.api.util.MySQL;
import de.sebpas.api.util.config.Config;
import de.sebpas.chat.ChatFormatter;
import de.sebpas.chat.TablistManager;

public class GameApi extends JavaPlugin{
	/**
	 * prefix strings
	 */
	public static final String prefix = ChatColor.translateAlternateColorCodes('�', "�a[Info]: �7");
	public static final String error = ChatColor.translateAlternateColorCodes('�', "�c[Fehler]: �7");
	
	/**
	 * manager sub classes
	 */
	private static GameApi instance;
	private static NickManager nickManager;
	private static ChatFormatter chatManager;
	private static GroupManager groupManager;
	private static TablistManager tablistManager;

	private GameSession gameSession;
	private MySQL sqlManager;
	
	@Override
	public void onEnable() {
		instance = this;
		sqlManager = new MySQL();
		
		this.getConfig().addDefault("GameApi.nick.source", "simple");
		this.getConfig().addDefault("GameApi.onlyapi", true);
		this.getConfig().addDefault("GameApi.nick.autonick", false);
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		
		this.reloadConfig();
		new Config(getConfig());
		
		nickManager = new NickManager();
		chatManager = new ChatFormatter();
		groupManager = new GroupManager(getConfig());
		tablistManager = new TablistManager();
		
		this.getCommand("nick").setExecutor(new CommandNick());
		CommandGameID cmdGameID;
		this.getCommand("gameid").setExecutor(cmdGameID = new CommandGameID());
		this.getCommand("session").setExecutor(cmdGameID);
		this.getCommand("chatlog").setExecutor(cmdGameID);
		this.getCommand("cl").setExecutor(cmdGameID);
		
		//TODO @Debug
			this.getServer().getPluginManager().registerEvents(new GameSessionListener(), instance);
	}
	
	@Override
	public void onDisable() {}
	
	public MySQL getSQLManager(){
		return sqlManager;
	}
	
	/**
	 * @return the groupmanager
	 */
	public static GroupManager getGroupManager(){
		return groupManager;
	}

	/**
	 * @return the chatmanager
	 */
	public static ChatManager getChatManager(){
		return chatManager;
	}

	/**
	 * @return an instance of this class
	 */
	public static GameApi getInstance() {
		return instance;
	}

	/**
	 * @return the current gamesession
	 */
	public GameSession getGameSession(){
		return gameSession;
	}
	
	/**
	 * @return the tablist prefix manager
	 */
	public static TablistManager getTablistManager(){
		return tablistManager;
	}

	/**
	 * @return the nickmanager
	 */
	public static NickManager getNickManager() {
		return nickManager;
	}
	
	public void setChatManager(ChatFormatter newManager){
		//HandlerList.unregisterAll(chatManager);
		chatManager = newManager;
		//Bukkit.getPluginManager().registerEvents(chatManager, instance);
	}
}
