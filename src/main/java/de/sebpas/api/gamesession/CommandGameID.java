package de.sebpas.api.gamesession;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.sebpas.api.GameApi;

public class CommandGameID implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			if(GameApi.getInstance().getGameSession() != null){
				GameSession gameSession = GameApi.getInstance().getGameSession();
				sender.sendMessage("[Info] Die aktuelle Game-ID: " + gameSession.getSessionId());
				System.out.println(gameSession.saveToString());
			}else{
				sender.sendMessage("[Fehler] Die GameId konnte nicht gefunden werden.");
			}
			return true;
		}
		Player p = (Player) sender;
		
		if(GameApi.getInstance().getGameSession() != null){
			GameSession gameSession = GameApi.getInstance().getGameSession();
			TextComponent t1 = new TextComponent("�a[Info] �7Die aktuelle �3GameID �7ist ");
			TextComponent t2 = new TextComponent("�3" + gameSession.getSessionId());
			t2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, gameSession.getSessionId()));
			TextComponent t3 = new TextComponent("�3Klicke, um die ID zu kopieren");
			t2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] { t3 }));
			p.spigot().sendMessage(t1, t2);
		}else{
			p.sendMessage("�c[Fehler] �7Die GameId konnte nicht gefunden werden.");
		}
		return true;
	}

}
