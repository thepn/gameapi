package de.sebpas.api.gamesession;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.sebpas.api.GameApi;
import de.sebpas.api.gamesession.Action.Type;

public class GameSessionListener implements Listener{
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		GameSession session = GameApi.getInstance().getGameSession();
		if(session != null){
			Action action = new Action(e.getPlayer().getUniqueId().toString(), Type.CHAT_MESSAGE);
			action.setArgs(e.getMessage());
			session.addAction(action);
		}
	}
}
