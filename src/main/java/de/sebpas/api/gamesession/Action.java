package de.sebpas.api.gamesession;


public class Action {
	public static enum Type{
		CHAT_MESSAGE,
		JOIN,
		LEAVE,
		CUSTOM,
		REPLAY_START;
	}
	
	private String target;
	private String[] args;
	private long time;
	private Type type;
	
	public Action(String target, Type type){
		this.type = type;
		this.time = System.currentTimeMillis();
		this.target = target;
	}
	
	public String getTarget() {
		return target;
	}
	
	public String[] getMessage() {
		return args;
	}
	
	public void setArgs(String... args) {
		this.args = args;
	}
	
	public long getTime() {
		return time;
	}

	public Type getType() {
		return type;
	}
}
