package de.sebpas.api.gamesession;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.google.gson.Gson;

import de.sebpas.api.GameApi;

public class GameSession {
	private final String sessionId;
	private final String gameModeName;

	private List<Action> actions;
	private List<Property> properties;
	
	private static void init(){
		final Connection c = GameApi.getInstance().getSQLManager().connect();
		if(c != null){
			try {
				PreparedStatement ps = c.prepareStatement("CREATE TABLE IF NOT EXISTS GameSessions (SessionId VARCHAR(13), Log TEXT, containsBan TINYINT)");
				ps.execute();
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public GameSession(String gameMode){
		this.gameModeName = gameMode;
		actions = new ArrayList<Action>();
		properties = new ArrayList<Property>();
		this.sessionId = getRandom(12);
		init();
		saveGameId();
	}
	
	public String saveToString(){
		return new Gson().toJson(this);
	}
	
	public void addAction(Action action) {
		actions.add(action);
	}
	
	public List<Action> getActions() {
		return actions;
	}
	
	public String getSessionId() {
		return sessionId;
	}
	
	public String getGameModeName() {
		return gameModeName;
	}
	
	/**
	 * @return a random generated string
	 */
	private static String getRandom(int length){
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890123456789---".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        return output;
    }
	
	public void saveGameId(){
		final Connection c = GameApi.getInstance().getSQLManager().connect();
		if(c != null){
			try {
				PreparedStatement ps = c.prepareStatement("INSERT INTO GameSessions (SessionId) VALUES (?)");
				ps.setString(1, sessionId);
				ps.execute();
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void saveGameLog(){
		final Connection c = GameApi.getInstance().getSQLManager().connect();
		if(c != null){
			try {
				PreparedStatement ps = c.prepareStatement("UPDATE GameSessions SET Log = ? WHERE SessionId = ?");
				ps.setString(1, saveToString());
				ps.setString(2, sessionId);
				ps.execute();
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<Property> getProperties() {
		return properties;
	}
}
