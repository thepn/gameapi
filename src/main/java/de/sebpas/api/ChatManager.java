package de.sebpas.api;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import de.sebpas.api.util.config.Config;
import de.sebpas.chat.IChatManager;

public abstract class ChatManager implements Listener, IChatManager{
	
	/** this is the chat format read from the config.yml
	 * %PLAYER% means player name;  Admin | thepn :
	 * %MSG% is the message^^  
	 */
	public static String chatFormat = "%PLAYER% &7> %MSG%";
	
	public ChatManager() {
		this.init();
		if(!Config.isApiOnly())
			Bukkit.getPluginManager().registerEvents(this, GameApi.getInstance());
	}

	/**
	 * this method loads the config: which permissions should cause which chat prefixes
	 */

	public void init(){
		System.out.println(GameApi.prefix + "Loading Config...");
		GameApi.getInstance().getConfig().addDefault("GameApi.chat.format", chatFormat);
		GameApi.getInstance().getConfig().options().copyDefaults(true);
		GameApi.getInstance().saveConfig();
		chatFormat = GameApi.getInstance().getConfig().getString("GameApi.chat.format");
	}
	
	/**
	 * events (join to set the displayname; asyncchat to set the chat format)
	 */
	
	@EventHandler (priority = EventPriority.LOWEST) /** lowest because nick events must be before! */
	public void onJoinEvent(PlayerJoinEvent e){
		Player p = e.getPlayer();
		if(GameApi.getNickManager().isNicked(e.getPlayer())){
			p.setDisplayName(this.getNickedName(p));
		}else{
			p.setDisplayName(this.getChatName(p));
		}
	}
	
	@EventHandler (priority = EventPriority.HIGH)
	public void onChatEvent(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		String msg = chatFormat.replace("%MSG%", "%2$s");
		msg = msg.replace("%PLAYER%", ChatColor.translateAlternateColorCodes('&', getChatName(p)));
		e.setFormat(ChatColor.translateAlternateColorCodes('&', msg));
		if(p.hasPermission("system.chat.color")){
			msg = ChatColor.translateAlternateColorCodes('&', e.getMessage());
			e.setMessage(msg);
		}
	}
	
	public abstract String getNameTag(Player p);
	
	public abstract String getChatName(Player p);
	
	public abstract String getNickedName(Player p);
	
	public abstract String getPlayerListName(Player p);
}
