package de.sebpas.api.util.exception;

import java.util.UUID;

public class PlayerNotFoundException extends Exception{

	/**
	 * serial id
	 */
	private static final long serialVersionUID = 1794044734381401358L;
	
	private String name;
	private UUID uuid;
	
	public PlayerNotFoundException(String player){
		super("Player " + player + " not found!");
		this.name = player;
	}

	public PlayerNotFoundException(UUID uuid){
		super("Player with uuid " + uuid.toString() + " not found!");
		this.uuid = uuid;
	}
	
	public PlayerNotFoundException(String player, UUID uuid){
		super("Player " + player + " not found!");
		this.uuid = uuid;
		this.name = player;
	}

	public String getName() {
		return name;
	}

	public UUID getUuid() {
		return uuid;
	}
}
