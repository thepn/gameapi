package de.sebpas.api.util.config;

import org.bukkit.configuration.file.FileConfiguration;

public class Config {
	private static Config instance;
	
	private static boolean onlyApi = true;
	
	private FileConfiguration cfg;
	public Config(FileConfiguration cfg){
		this.cfg = cfg;
		instance = this;
		onlyApi = instance.cfg.getBoolean("GameApi.onlyapi");
	}
	
	public static String getEntry(String path){
		return instance.cfg.getString(path);
	}
	
	public static boolean getBoolean(String path){
		return instance.cfg.getBoolean(path);
	}
	
	public static void setApi(boolean onlyApi){
		Config.onlyApi = onlyApi;
	}
	
	public static boolean isApiOnly(){
		return onlyApi;
	}
}
