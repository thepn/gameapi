package de.sebpas.api.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.comphenix.packetwrapper.AbstractPacket;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

public class PacketUtil {
	public static void sendToAll(AbstractPacket packet){
		for(Player p : Bukkit.getOnlinePlayers()){
			packet.sendPacket(p);
		}
	}
	
	public static void send(AbstractPacket packet, Player p){
		packet.sendPacket(p);
	}
	
	public static WrappedGameProfile convertIntoWrapper(GameProfile old){
		WrappedGameProfile gp = new WrappedGameProfile(old.getId(), old.getName());
		for(String s : old.getProperties().keySet()){
			if(!gp.getProperties().containsKey(s))
				for(Property p : old.getProperties().get(s))
					gp.getProperties().put(s, new WrappedSignedProperty(p.getName(), p.getValue(), p.getSignature()));
		}
		return gp;
	}
}
