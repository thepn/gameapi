package de.sebpas.api.util;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerListHeaderFooter;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.google.common.collect.Lists;

public class IngameUtil {

	private static String prefix;
	
	public static void broadcastLevel(int xp, List<Player> list){
		for(Player p : list){
			p.setLevel(xp);
		}
	}
	
	public static void broadcastLevel(int xp){
		broadcastLevel(xp, Lists.newArrayList(Bukkit.getOnlinePlayers()));
	}
	
	public static void broadcastMessage(String msg, List<Player> list){
		for(Player p : list){
			p.sendMessage(prefix + msg);
		}
	}
	
	public static void broadcastMessage(String msg){
		broadcastMessage(msg, Lists.newArrayList(Bukkit.getOnlinePlayers()));
	}

	/**
	 * @return the current chat prefix used for plugin messages
	 */
	public static String getPrefix() {
		return prefix;
	}

	public static void setPrefix(String prefix) {
		IngameUtil.prefix = prefix;
	}
	
	/**
	 * sends the tablist header and footer to player p
	 * @param header
	 * @param footer
	 * @param p
	 */
	public static void sendTablistHeaderFooter(String header, String footer, Player p){
		WrapperPlayServerPlayerListHeaderFooter packet = new WrapperPlayServerPlayerListHeaderFooter();
		packet.setHeader(WrappedChatComponent.fromText(ChatColor.translateAlternateColorCodes('&', header)));
		packet.setFooter(WrappedChatComponent.fromText(ChatColor.translateAlternateColorCodes('&', footer)));
		packet.sendPacket(p);
	}
}
