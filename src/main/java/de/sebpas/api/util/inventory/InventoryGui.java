package de.sebpas.api.util.inventory;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import com.google.common.collect.Lists;

import de.sebpas.api.GameApi;

public abstract class InventoryGui implements Listener{

	protected List<GuiAction> buttonActions;
	protected Inventory inv;
	
	
	public InventoryGui(String title, int size, Player holder){
		buttonActions = Lists.newArrayList();
		this.inv = Bukkit.createInventory(holder, size, title);
		Bukkit.getPluginManager().registerEvents(this, GameApi.getInstance());
	}
	
	/**
	 * opens the gui
	 */
	public abstract void open(Player p);
	
	@EventHandler
	public void onInventoryInteract(InventoryClickEvent e){
		if(!e.getClickedInventory().equals(inv))
			return;
		e.setCancelled(true);
		if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getItemMeta().getDisplayName() != null){
			String displayName = e.getCurrentItem().getItemMeta().getDisplayName();
			for(GuiAction a : buttonActions){
				if(displayName.contains(a.getTriggerName())){
					if(a.getClickType() == e.getClick()){
						a.doAction((Player) e.getWhoClicked()); 
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent e){
		if(e.getInventory().equals(inv)){
			Bukkit.broadcastMessage("Closed inv!");
			e.getHandlers().unregister(this);
		}
	}
	
	public Inventory getInventory(){
		return inv;
	}
}
