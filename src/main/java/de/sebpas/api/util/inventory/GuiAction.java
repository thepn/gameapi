package de.sebpas.api.util.inventory;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

public abstract class GuiAction {
	
	private String triggerName;
	private ClickType clickType;
	
	public GuiAction(String triggerName, ClickType clickType){
		this.triggerName = triggerName;
		this.clickType = clickType;
	}
	
	public abstract void doAction(Player p);

	public String getTriggerName() {
		return triggerName;
	}

	public ClickType getClickType() {
		return clickType;
	}
}
