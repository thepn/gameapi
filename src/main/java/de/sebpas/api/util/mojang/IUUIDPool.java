package de.sebpas.api.util.mojang;

import java.util.UUID;

public interface IUUIDPool {
	
	/**
	 * init method if needed
	 */
	void init();
	
	/**
	 * return true if the pool is connected to its source
	 */
	boolean isConnected();
	
	
	/**
	 * returns a uuid from the player name
	 */
	UUID getUUID(String name);
	
	/**
	 * returns the name from the uuid
	 */
	String getName(UUID uuid);
}
