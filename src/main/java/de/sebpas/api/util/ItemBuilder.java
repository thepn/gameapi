package de.sebpas.api.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.inventory.meta.SpawnEggMeta;
import org.bukkit.material.Dye;
import org.bukkit.material.SpawnEgg;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

public class ItemBuilder {
	private ItemStack theItem;
	
	public ItemBuilder(){
		theItem = new ItemStack(Material.AIR);
	}
	
	public ItemBuilder(ItemStack item){
		this.theItem = item;
	}
	
	public ItemBuilder(Material material){
		theItem = new ItemStack(material);
	}
	
	public ItemBuilder setColor(DyeColor color){
		Dye dye = new Dye();
		dye.setColor(color);
		theItem.setItemMeta(dye.toItemStack().getItemMeta());
		theItem.setData(dye.toItemStack().getData());
		return this;
	}
	
	public ItemBuilder setDurability(short durability){
		theItem.setDurability(durability);
		return this;
	}
	
	public ItemBuilder setAmount(int amount){
		theItem.setAmount(amount);
		return this;
	}
	
	public ItemBuilder setDisplayName(String displayname){
		ItemMeta meta = theItem.getItemMeta();
		meta.setDisplayName(displayname);
		theItem.setItemMeta(meta);
		return this;
	}
	
	/** seperate with \n */
	public ItemBuilder setLore(String lore){
		List<String> lines = new ArrayList<String>();
		String[] split = lore.split("\n");
		
		for(int i = 0; i < split.length; i++){
			lines.add(split[i]);
		}
		ItemMeta meta = theItem.getItemMeta();
		meta.setLore(lines);
		theItem.setItemMeta(meta);
		return this;
	}
	
	/** to use unsafe enchantments set unsafe to true */
	public ItemBuilder addEnchantment(Enchantment enchantment, int level, boolean unsafe){
		if(unsafe){
			theItem.addUnsafeEnchantment(enchantment, level);
		}else{
			theItem.addEnchantment(enchantment, level);
		}
		return this;
	}
	
	/**
	 * sets the skull owner using player name
	 * @param owner
	 * @return ItemStack
	 */
	public ItemBuilder setSkull(String owner){
		theItem = new ItemStack(Material.PLAYER_HEAD, theItem.getAmount(), (short) 3);
		SkullMeta meta = (SkullMeta) theItem.getItemMeta();
		meta.setOwningPlayer(Bukkit.getOfflinePlayer(Bukkit.getPlayer(owner).getUniqueId()));
		theItem.setItemMeta(meta);
		return this;
	}
	
	/**
	 * sets the texture of the skull using base64 encrypted texture
	 * @param textureid
	 * @return ItemStack
	 */
	public ItemBuilder setSkullByTexture(String textureid){
		theItem = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
		SkullMeta meta = (SkullMeta) theItem.getItemMeta();
		GameProfile gp = new GameProfile(UUID.randomUUID(), null);
		gp.getProperties().put("textures", new Property("textures", textureid));
		Field f = null;
		try{
			f = meta.getClass().getDeclaredField("profile");
			f.setAccessible(true);
			f.set(meta, gp);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		theItem.setItemMeta(meta);
		return this;
	}
	
	public String toString(){
		return theItem.toString();
	}
	
	public ItemBuilder clone(){
		return new ItemBuilder(theItem.clone());
	}
	
	public void reset(){
		theItem = new ItemStack(Material.AIR);
	}
	
	public ItemStack build(){
		return theItem;
	}
}
