package de.sebpas.api.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Reflections {
	
	public static void setValue(Object obj, String name, Object value){
		try{
			Field field = obj.getClass().getDeclaredField(name);
			field.setAccessible(true);
			field.set(obj, value);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static Object getValue(Object obj, String name){
		try{
			Field field = obj.getClass().getDeclaredField(name);
			field.setAccessible(true);
			return field.get(obj);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public static Class<?> getClass(String className) {
		String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
		String name = "net.minecraft.server." + version + className;
		Class<?> clazz;
		try {
			clazz = Class.forName(name);
			return clazz;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	public void sendPacket(Object packet, Player player){
		Method getHandleMethod;
		try {
			getHandleMethod = player.getClass().getMethod("getHandle");
			Object nmsPlayerObj = getHandleMethod.invoke(player);
			Field conField = nmsPlayerObj.getClass().getField("playerConnection");
			Object con = conField.get(nmsPlayerObj);
			
			Method sendPacket = getClass("PlayerConnection").getMethod("sendPacket", getClass("Packet"));
			sendPacket.invoke(con, packet);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
	}
	public void sendPacket(Object packet){
		for(Player p : Bukkit.getOnlinePlayers()){
			this.sendPacket(packet, p);
		}
	}
	public Method getStaticMethod(Class<?> type, String methodName, Class<?>... params) {
		try {
			Method method = type.getDeclaredMethod(methodName, params);
			if ((method.getModifiers() & Modifier.STATIC) != 0) {
				return method;
			}
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return null;
	}
}
