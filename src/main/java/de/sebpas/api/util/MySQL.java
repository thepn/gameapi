package de.sebpas.api.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.bukkit.configuration.file.FileConfiguration;

import de.sebpas.api.GameApi;

public class MySQL {
	private String password;
	private String host, database;
	private String username;
	
	public MySQL(){
		FileConfiguration cfg = GameApi.getInstance().getConfig();
		cfg.addDefault("sql.host", "localhost:3306");
		cfg.addDefault("sql.username", "lobbydatabase");
		cfg.addDefault("sql.password", "secretpassword");
		cfg.addDefault("sql.database", "lobbydatabase");
		cfg.options().copyDefaults(true);
		GameApi.getInstance().saveDefaultConfig();
		
		if(cfg.contains("sql.host"))
			host = cfg.getString("sql.host");
		if(cfg.contains("sql.username"))
			username = cfg.getString("sql.username");
		if(cfg.contains("sql.password"))	
			password = cfg.getString("sql.password");
		if(cfg.contains("sql.database"))
			database = cfg.getString("sql.database");
	}
	/**
	 * connects the mysql connection if it is <b>not</b> connected yet.
	 */
	public Connection connect(){
		if(host == null || database == null || username == null || password == null)
			return null;
		try {
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				System.err.println("Mysql driver not found :(");
				return null;
			}
			return DriverManager.getConnection("jdbc:mysql://" + host + "/" + database + "?autoReconnect=true", username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
