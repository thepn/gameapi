package de.sebpas.api.util;

import org.bukkit.entity.Player;

import com.comphenix.packetwrapper.WrapperPlayServerTitle;
import com.comphenix.protocol.wrappers.EnumWrappers.TitleAction;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

/**
 * this class is a utility class which is able to send title packets to players
 * @author thepn
 */
public class Title {
	
	public static void sendTitle(Player p, String title, String subtitle, int fadeIn, int stay, int fadeOut){
		sendTimings(p, fadeIn, stay, fadeOut);
		sendTitle(p, title, subtitle);
	}
	
	public static void sendTitle(Player p, String title, String subtitle){
		sendSubTitle(p, subtitle);
		sendTitle(p, title);
	}
	
	public static void sendTitle(Player p, String title){
        WrapperPlayServerTitle packet = new WrapperPlayServerTitle();
        packet.setAction(TitleAction.TITLE);
        packet.setTitle(WrappedChatComponent.fromText(title));
        packet.sendPacket(p);
	}
	
	public static void sendTimings(Player p, int in, int stay, int out){
		WrapperPlayServerTitle packet = new WrapperPlayServerTitle();
        packet.setAction(TitleAction.TIMES);
        packet.setFadeIn(in);
        packet.setStay(stay);
        packet.setFadeOut(out);
        packet.sendPacket(p);
	}
	
	public static void sendReset(Player p){
		WrapperPlayServerTitle packet = new WrapperPlayServerTitle();
        packet.setAction(TitleAction.RESET);
        packet.sendPacket(p);
	}
	
	public static void sendSubTitle(Player p, String title){
		WrapperPlayServerTitle packet = new WrapperPlayServerTitle();
        packet.setAction(TitleAction.SUBTITLE);
        packet.setTitle(WrappedChatComponent.fromText(title));
        packet.sendPacket(p);
	}
}
