package de.sebpas.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerNamedEntitySpawn;
import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.protocol.wrappers.EnumWrappers.NativeGameMode;
import com.comphenix.protocol.wrappers.EnumWrappers.PlayerInfoAction;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.sebpas.api.nick.INickPool;
import de.sebpas.api.nick.NickData;
import de.sebpas.api.nick.NickListener;
import de.sebpas.api.nick.PlayerNickEvent;
import de.sebpas.api.nick.PlayerNickRemovedEvent;
import de.sebpas.api.nick.nickpools.ConfigNickPool;
import de.sebpas.api.nick.nickpools.SimpleNickPool;
import de.sebpas.api.util.PacketUtil;
import de.sebpas.api.util.config.Config;
import de.sebpas.api.util.exception.PlayerNotFoundException;
import de.sebpas.api.util.mojang.GameProfileFetcher;

public class NickManager {
	
	private Map<Player, NickData> nicked;
	
	public static boolean autoNick = false;
	
	//TODO add a few more sources: mysql; config; ...
	private INickPool nickPool;
	
	public NickManager(){
		this.nicked = Maps.newHashMap();
		if(!Config.isApiOnly()){
			if(Config.getEntry("GameApi.nick.source").equalsIgnoreCase("simple")){
				nickPool = new SimpleNickPool();
			}
			if(Config.getEntry("GameApi.nick.source").equalsIgnoreCase("config")){
				nickPool = new ConfigNickPool();
			}
			autoNick = Config.getBoolean("GameApi.nick.autonick");
		}
		
		new NickListener().start();
	}
	
	public List<Player> getNickedPlayers(){
		return Lists.newArrayList(nicked.keySet());
	}

	public boolean isNicked(Player p) {
		return nicked.containsKey(p);
	}
	
	public NickData getNickName(UUID uuid){
		for(Player p : nicked.keySet())
			if(p.getUniqueId().equals(uuid))
				return nicked.get(p);
		return null;
	}

	public NickData getNickName(String name){
		for(Player p : nicked.keySet())
			if(p.getName().equalsIgnoreCase(name))
				return nicked.get(p);
		return null;
	}
	
	public void unNick(final Player p, boolean getNewNickName){
		if(!nicked.containsKey(p))
			return;
		
		final NickData nickData = nicked.get(p);
		
		GameApi.getChatManager().updateDisplayName(p);
		
		WrapperPlayServerEntityDestroy destroy = new WrapperPlayServerEntityDestroy();
		WrapperPlayServerPlayerInfo tabremove = new WrapperPlayServerPlayerInfo();
		tabremove.setAction(PlayerInfoAction.REMOVE_PLAYER);
		List<PlayerInfoData> list = Lists.newArrayList();
		list.add(new PlayerInfoData(new WrappedGameProfile(nicked.get(p).getUuid(), nicked.get(p).getNickName()), 0, NativeGameMode.NOT_SET, WrappedChatComponent.fromText("")));
		tabremove.setData(list);
		destroy.setEntityIds(new int[] { p.getEntityId() });
		
		for(Player t : Bukkit.getOnlinePlayers()){
			if(t.getName().equals(p.getName()) || t.hasPermission("nick"))
				continue;
			destroy.sendPacket(t);
			tabremove.sendPacket(t);
		}
		
		nicked.remove(p);
		
		WrapperPlayServerNamedEntitySpawn spawn = new WrapperPlayServerNamedEntitySpawn();
		spawn.setEntityID(p.getEntityId());
		spawn.getHandle().getUUIDs().write(0, p.getUniqueId());
		spawn.getHandle().getIntegers().write(1, (int) Math.floor(p.getLocation().getX() * 32.0D));
		spawn.getHandle().getIntegers().write(2, (int) Math.floor(p.getLocation().getY() * 32.0D));
		spawn.getHandle().getIntegers().write(3, (int) Math.floor(p.getLocation().getZ() * 32.0D));
		spawn.getHandle().getBytes().write(0, (byte) (p.getLocation().getYaw() * 256F / 260F));
		spawn.getHandle().getBytes().write(1, (byte) (p.getLocation().getPitch() * 256F / 260F));
		WrappedDataWatcher w = new WrappedDataWatcher();
		w.setObject(6, (float) 20);
		spawn.getHandle().getDataWatcherModifier().write(0, w);
		
		WrapperPlayServerPlayerInfo tabadd = new WrapperPlayServerPlayerInfo();
		tabadd.setAction(PlayerInfoAction.ADD_PLAYER);
		List<PlayerInfoData> data = new ArrayList<PlayerInfoData>();
		try {
			data.add(new PlayerInfoData(PacketUtil.convertIntoWrapper(GameProfileFetcher.fetch(p.getUniqueId())), 1, NativeGameMode.NOT_SET, WrappedChatComponent.fromText(p.getName())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		tabadd.setData(data);
		for(Player t : Bukkit.getOnlinePlayers()){
			if(t.getName().equals(p.getName()) || t.hasPermission("nick"))
				continue;
			tabadd.sendPacket(t);
			spawn.sendPacket(t);
		}
		if(!getNewNickName){
			new BukkitRunnable() {
				
				@Override
				public void run() {
					for(Player t : Bukkit.getOnlinePlayers())
						if(!t.getName().equals(p.getName()) && !t.hasPermission("nick"))
							t.hidePlayer(p);
					
					new BukkitRunnable() {
						
						@Override
						public void run() {
							for(Player t : Bukkit.getOnlinePlayers())
								if(!t.getName().equals(p.getName()) && !t.hasPermission("nick"))
									t.showPlayer(p);
							
						}
					}.runTaskLater(GameApi.getInstance(), 2);
				}
			}.runTaskLater(GameApi.getInstance(), 5);
			Bukkit.getPluginManager().callEvent(new PlayerNickRemovedEvent(nickData, p));
		}
	}
	
	/**
	 * nicks the player (changes the skin and the name)
	 * @param p
	 * @param nickData
	 * @throws PlayerNotFoundException
	 */
	public void nick(final Player p, NickData nickData) throws PlayerNotFoundException{
		unNick(p, true);

		WrapperPlayServerEntityDestroy destroy = new WrapperPlayServerEntityDestroy();
		WrapperPlayServerPlayerInfo tabremove = new WrapperPlayServerPlayerInfo();
		tabremove.setAction(PlayerInfoAction.REMOVE_PLAYER);
		List<PlayerInfoData> list = Lists.newArrayList();
		list.add(new PlayerInfoData(new WrappedGameProfile(p.getUniqueId(), p.getName()), 0, NativeGameMode.NOT_SET, WrappedChatComponent.fromText("")));
		tabremove.setData(list);
		destroy.setEntityIds(new int[] { p.getEntityId() });
		
		for(Player t : Bukkit.getOnlinePlayers()){
			if(t.getName().equals(p.getName()) || t.hasPermission("nick"))
				continue;
			destroy.sendPacket(t);
			tabremove.sendPacket(t);
		}
		if(nickData == null)
			throw new PlayerNotFoundException("null");
		nicked.put(p, nickData);
		GameApi.getChatManager().updateDisplayName(p);
		
		WrapperPlayServerNamedEntitySpawn spawn = new WrapperPlayServerNamedEntitySpawn();
		spawn.setEntityID(p.getEntityId());
		spawn.getHandle().getUUIDs().write(0, nickData.getUuid());
		spawn.getHandle().getIntegers().write(1, (int) Math.floor(p.getLocation().getX() * 32.0D));
		spawn.getHandle().getIntegers().write(2, (int) Math.floor(p.getLocation().getY() * 32.0D));
		spawn.getHandle().getIntegers().write(3, (int) Math.floor(p.getLocation().getZ() * 32.0D));
		spawn.getHandle().getBytes().write(0, (byte) (p.getLocation().getYaw() * 256F / 260F));
		spawn.getHandle().getBytes().write(1, (byte) (p.getLocation().getPitch() * 256F / 260F));
		WrappedDataWatcher w = new WrappedDataWatcher();
		w.setObject(6, (float) 20);
		spawn.getHandle().getDataWatcherModifier().write(0, w);
		
		WrapperPlayServerPlayerInfo tabadd = new WrapperPlayServerPlayerInfo();
		tabadd.setAction(PlayerInfoAction.ADD_PLAYER);
		List<PlayerInfoData> data = new ArrayList<PlayerInfoData>();
		data.add(new PlayerInfoData(nickData.getWrappedGameProfile(), 1, NativeGameMode.NOT_SET, WrappedChatComponent.fromText(p.getName())));
		tabadd.setData(data);
		for(Player t : Bukkit.getOnlinePlayers()){
			if(t.getName().equals(p.getName()) || t.hasPermission("nick"))
				continue;
			tabadd.sendPacket(t);
			spawn.sendPacket(t);
		}
		new BukkitRunnable() {
			
			@Override
			public void run() {
				for(Player t : Bukkit.getOnlinePlayers())
					if(!t.getName().equals(p.getName()) && !t.hasPermission("nick"))
						t.hidePlayer(p);
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						for(Player t : Bukkit.getOnlinePlayers())
							if(!t.getName().equals(p.getName()) && !t.hasPermission("nick"))
								t.showPlayer(p);
						
					}
				}.runTaskLater(GameApi.getInstance(), 2);
			}
		}.runTaskLater(GameApi.getInstance(), 5);
		Bukkit.getPluginManager().callEvent(new PlayerNickEvent(nickData, p));
	}
	
	public void nick(Player p, String nick, UUID uuid) throws PlayerNotFoundException{
		nick(p, new NickData(nick, uuid));
	}
	
	public boolean isNicked(UUID uuid) {
		for(Player p : nicked.keySet())
			if(p.getUniqueId().equals(uuid))
				return true;
		return false;
	}
	
	public boolean isNicked(String name) {
		for(Player p : nicked.keySet())
			if(p.getName().equalsIgnoreCase(name))
				return true;
		return false;
	}

	public INickPool getNickPool() {
		return nickPool;
	}

	public void setNickPool(INickPool uuidPool) {
		this.nickPool = uuidPool;
	}
}
