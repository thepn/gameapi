package de.sebpas.chat;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.google.common.collect.Lists;

import de.sebpas.api.GameApi;

public class TablistManager {
	private Map<Player, String> prefixes;
	private Map<Player, String> suffixes;
	
	public TablistManager(){
		prefixes = new HashMap<Player, String>();
		suffixes = new HashMap<Player, String>();
	}

	public Map<Player, String> getPrefixes() {
		return prefixes;
	}

	public Map<Player, String> getSuffixes() {
		return suffixes;
	}
	
	public void setSuffix(Player p, String suffix){
		if(suffixes.containsKey(p))
			suffixes.remove(p);
		suffixes.put(p, suffix);
	}
	
	public void removeSuffix(Player p){
		if(suffixes.containsKey(p))
			suffixes.remove(p);
	}
	
	public void setPrefix(Player p, String prefix){
		if(prefixes.containsKey(p))
			prefixes.remove(p);
		prefixes.put(p, prefix);
	}
	
	public void removePrefix(Player p){
		if(prefixes.containsKey(p))
			prefixes.remove(p);
	}
	
	/**
	 * sends scoreboards with the "pseudo" prefix 
	 * @param prefix
	 * @param suffix
	 * @param p
	 * @param targets
	 */
	public void update(String prefix, String suffix, Player p, List<Player> targets){
		System.out.println("sent");
		PacketContainer remove = new PacketContainer(PacketType.Play.Server.SCOREBOARD_TEAM);
		remove.getIntegers().write(1, 1);
		remove.getStrings().write(1, GameApi.getGroupManager().getGroup(p).getIndex() + p.getName());
		remove.getStrings().write(0, GameApi.getGroupManager().getGroup(p).getIndex() + p.getName());
		
		PacketContainer add = new PacketContainer(PacketType.Play.Server.SCOREBOARD_TEAM);
		add.getIntegers().write(1, 0);
		add.getStrings().write(1, GameApi.getGroupManager().getGroup(p).getIndex() + p.getName());
		add.getStrings().write(0, GameApi.getGroupManager().getGroup(p).getIndex() + p.getName());
		add.getStrings().write(2, prefix);
		add.getStrings().write(3, suffix);
		
		PacketContainer addPlayer = new PacketContainer(PacketType.Play.Server.SCOREBOARD_TEAM);
		addPlayer.getIntegers().write(1, 3);
		addPlayer.getStrings().write(1, GameApi.getGroupManager().getGroup(p).getIndex() + p.getName());
		addPlayer.getStrings().write(0, GameApi.getGroupManager().getGroup(p).getIndex() + p.getName());
		List<String> list = Lists.newArrayList();
		list.add(p.getName());
		addPlayer.getSpecificModifier(Collection.class).write(0, list);
		
		for(Player t : targets){
			try {
				ProtocolLibrary.getProtocolManager().sendServerPacket(t, remove);
				ProtocolLibrary.getProtocolManager().sendServerPacket(t, add);
				ProtocolLibrary.getProtocolManager().sendServerPacket(t, addPlayer);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void start(){
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(GameApi.getInstance(), PacketType.Play.Server.SCOREBOARD_TEAM){
			@Override
			public void onPacketSending(PacketEvent event){
				if(event.getPacketType() == PacketType.Play.Server.SCOREBOARD_TEAM){}
			}
		});
	}
}
