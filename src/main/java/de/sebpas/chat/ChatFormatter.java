package de.sebpas.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import de.sebpas.api.ChatManager;
import de.sebpas.api.GameApi;

public class ChatFormatter extends ChatManager{
	/**
	 * use chat.method() to get access to all non-static methods of this (super) class
	 */
	
	/**
	 * @param p
	 * @return the player's name which will be shown on their nametag / their tablist prefix
	 */
	public String getNameTag(Player p){
		if(GameApi.getNickManager().isNicked(p)){
			return GameApi.getGroupManager().getDefaultGroup().getNameTagPrefix().replace("%NAME%", GameApi.getNickManager().getNickName(p.getUniqueId()).getNickName());
		}
		
		return GameApi.getGroupManager().getGroup(p).getNameTagPrefix().replace("%NAME%", p.getName());
	}

	@Override
	public String getChatName(Player p) {
		if(GameApi.getNickManager().isNicked(p)){
			return GameApi.getGroupManager().getDefaultGroup().getChatPrefix().replace("%NAME%", GameApi.getNickManager().getNickName(p.getUniqueId()).getNickName());
		}
		
		return GameApi.getGroupManager().getGroup(p).getChatPrefix().replace("%NAME%", p.getName());
	}

	@Override
	public String getNickedName(Player p) {
		return GameApi.getGroupManager().getDefaultGroup().getChatPrefix().replace("%NAME%", GameApi.getNickManager().getNickName(p.getName()).getNickName());
	}

	@Override
	@EventHandler
	public void onChatEvent(AsyncPlayerChatEvent e) {
		super.onChatEvent(e);
	}

	@Override
	@EventHandler
	public void onJoinEvent(PlayerJoinEvent e) {
		for(Player p : Bukkit.getOnlinePlayers()){
			Scoreboard sm = p.getScoreboard();
			Team t = sm.getTeam(e.getPlayer().getName());
			if(t == null)
				t = sm.registerNewTeam(e.getPlayer().getName());
			t.setPrefix(ChatColor.translateAlternateColorCodes('&', getPlayerListName(e.getPlayer())));
			p.setScoreboard(sm);
		}
		super.onJoinEvent(e);
	}

	@Override
	public void updateDisplayName(Player p) {
		if(GameApi.getNickManager().isNicked(p)){
			p.setDisplayName(ChatColor.translateAlternateColorCodes('&', this.getNickedName(p)));
		}else{
			p.setDisplayName(ChatColor.translateAlternateColorCodes('&', this.getChatName(p)));
		}
	}

	@Override
	public String getPlayerListName(Player p) {
		if(GameApi.getNickManager().isNicked(p)){
			return GameApi.getGroupManager().getDefaultGroup().getNameTagPrefix() + GameApi.getNickManager().getNickName(p.getUniqueId()).getNickName() + GameApi.getGroupManager().getDefaultGroup().getNameTagSuffix();
		}
		
		return GameApi.getGroupManager().getDefaultGroup().getNameTagPrefix() + p.getName() + GameApi.getGroupManager().getDefaultGroup().getNameTagSuffix();
	}
}
