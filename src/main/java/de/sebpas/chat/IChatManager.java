package de.sebpas.chat;

import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public interface IChatManager {
	void init();
	void onChatEvent(AsyncPlayerChatEvent e);
	void onJoinEvent(PlayerJoinEvent e);
	
	void updateDisplayName(Player p);
}
